package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;



public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {

	public class Node<E> { 
		private E element;
		private Node<E> next, prev;

		public Node(E e, Node<E> prev, Node<E> next) {
			this.element = e;
			this.prev = prev;
			this.next = next;
		}

		public Node() {
			this(null,null,null);
		}
		
		public Node(E e) {
			this(e,null,null);
		}

		private void clean() {
			this.element = null;
			this.prev = null;
			this.next = null;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	public class ListForwardIterator<E> implements Iterator<E>{

		private Node<E> cursor;

		//This constructor will initialize the cursor at the given index
		public ListForwardIterator(int index){
			checkIndex(index);
			this.cursor = (Node<E>) getNode(index);
		}
		
		//This constructor will initialize the cursor at the first element
		public ListForwardIterator() {
			this(0);
		}

		@Override
		public boolean hasNext() {
			return this.cursor != header;
		}

		@Override
		public E next() {
			while(hasNext()) {
				E result = this.cursor.getElement();
				this.cursor = this.cursor.getNext();
				return result;
			}

			throw new NoSuchElementException();
		}
	}
	
	public class ListReverseIterator<E> implements ReverseIterator<E>{
		
		private Node<E> cursor;
		
		//This constructor will initialize the cursor at the given index
		public ListReverseIterator(int index) {
			checkIndex(index);
			this.cursor = (Node<E>) getNode(index);
		}
		
		//This constructor will initialize the cursor at last element
		public ListReverseIterator() {
			this(size() -1);
		}

		@Override
		public boolean hasPrevious() {
			return this.cursor != header;
		}

		@Override
		public E previous() {
			while(hasPrevious()) {
				E result = this.cursor.getElement();
				this.cursor = this.cursor.getPrev();
				return result;
			}
			
			throw new NoSuchElementException();
		}
		
	}

	private int currentSize;
	private Node<E> header;

	public SortedCircularDoublyLinkedList() {
		this.currentSize = 0;
		this.header = new Node<E>();
		this.header.setNext(header);
		this.header.setPrev(header);
	}
	
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}


	@Override
	public boolean add(E obj) { 

		//if list is empty add a new element
		if(isEmpty()) {
			addBetween(new Node<E>(obj, header, header.getNext()), header, header.getNext());;
			currentSize++;
			return true;
		}

		/*we use two nodes, nta to create node whose element will be added and curr to examine
		which node we're at. We start with the first node in the list. */
		else {
			Node<E> nta = new Node<E>(obj);
			Node<E> curr = getNode(0);

			/* We now compare  the node elements while current is not the header, if current IS header
			 * we add the node at the end*/
			while(curr != header) {
				
				/*if the comparison gives a zero or negative value we add the new element before 
				 * the current node*/
				if(nta.getElement().compareTo(curr.getElement()) <= 0) {
					addBetween(nta, curr.getPrev(), curr);
					currentSize++;
					return true;
				} 
				
				//If the comparison gives a positive value we move the current node
				else if(curr.getNext() != header) {
					curr = curr.getNext();
				}
				
				//This method adds new element when its larger than all the previous ones
				else { 
					addBetween(nta, header.getPrev(), header);
					currentSize++;
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean remove(E obj) {
		//if the list is empty or it does not contain the element to remove return false
		if(isEmpty() || !this.contains(obj)) return false;

		Node<E> ntr = header.getNext();
		
		while(ntr.getElement() != obj) {
			ntr = ntr.getNext();
		}
		if(ntr.equals(header)) return false;

		ntr.getPrev().setNext(ntr.getNext());
		ntr.getNext().setPrev(ntr.getPrev());
		ntr.clean();
		currentSize--;
		return true;
	}

	@Override
	public boolean remove(int index) {
		/*if the list is empty returns null and then checks the index to see if 
		 * it is valid */
		if(isEmpty()) return false;
		checkIndex(index);
		
		Node<E> ntr = getNode(index);
		ntr.getPrev().setNext(ntr.getNext());
		ntr.getNext().setPrev(ntr.getPrev());
		ntr.clean();
		currentSize--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		if(isEmpty()) return 0;
		int count = 0;
		while(contains(obj)) {
			remove(obj);
			count++;
		}

		return count;
	}

	@Override
	public E first() {
		if(isEmpty()) return null;
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		if(isEmpty()) return null;
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		checkIndex(index);
		return getNode(index).getElement();
	}

	@Override
	public void clear() {
		while(!isEmpty())
			remove(0);
	}

	@Override
	public boolean contains(E e) {
		for(E i: this) {
			if(i.equals(e))
				return true;
		}
		return false;
	}



	@Override
	public int firstIndex(E e) {
		for(int i = 0; i<size(); i++) {
			if(get(i).equals(e))
				return i;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		for(int i = size()-1; i >= 0; i--) {
			if(get(i).equals(e))
				return i;
		}
		return -1;
	}
	
	@Override
	public Iterator<E> iterator(int index) {
		return new ListForwardIterator<E>(index);
	}

	@Override
	public Iterator<E> iterator() {
		return new ListForwardIterator<E>();
	}
	
	@Override
	public ReverseIterator<E> reverseIterator() {
		return new ListReverseIterator<E>();
	}
	
	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new ListReverseIterator<E>(index);
	}

	private Node<E> getNode(int index){
		checkIndex(index);
		int pos = 0;
		Node<E> current = header.getNext();
		while(pos != index) {
			current = current.getNext();
			pos++;
		}
		return current;
	}
	
	private void addBetween(Node<E> nta, Node<E> prev, Node<E> next) {
		nta.setNext(next);
		nta.setPrev(prev);
		prev.setNext(nta);
		next.setPrev(nta);
	}

	private void checkIndex(int index) {
		if(index < 0 || index > size()) {
			throw new IndexOutOfBoundsException("Index is not in the list.");
		}
	}


}
